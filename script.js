let btn_add_nota = document.querySelector("#add-nota-btn")
btn_add_nota.addEventListener("click", adicionarNota)

let btn_calcular = document.querySelector("#calcular-btn");
btn_calcular.addEventListener("click", calculaMedia)
 
let contador = 1; 
let media = 0;
function adicionarNota() {
    let input = document.querySelector("#input-nota")
    let nota = input.value;
    console.log(nota)
    input.value = ""
    nota = parseFloat(nota)

    if(nota === "" || isNaN(nota)) {
        return alert("Por favor, insira uma nota")
    } else if (nota < 0 || nota > 10 ) {
        return alert("A nota digitada é inválida, por favor, insira uma nota válida")
    } else {
        let campo_notas = document.querySelector(".notas")

        let span_notas = document.createElement("span")
        span_notas.innerHTML = "A nota " + contador + " foi " + nota

        campo_notas.append(span_notas)

        contador += 1

        media += nota
    }
    
}

function calculaMedia() {
    media = (media / (contador - 1)).toFixed(2)
    console.log(media)

    document.querySelector("#media-paragrafo").innerText += media
}